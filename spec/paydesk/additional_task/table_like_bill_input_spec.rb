# frozen_string_literal: true

require './lib/paydesk'

describe Paydesk do
  let!(:bills_hash) { "bill\tamount\n100\t2\n20\t3\n5\t1\n1\t15" }

  it 'returns expected hash' do
    skip unless Paydesk::ADDITIONAL_TASK

    expect(described_class.new(bills_hash, 125).call).to eq("bill\tamount\n100\t1\n20\t1\n5\t1")
  end
end
