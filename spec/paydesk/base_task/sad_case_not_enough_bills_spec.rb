# frozen_string_literal: true

require './lib/paydesk'

describe Paydesk do
  let!(:bills_hash) { { '50': 1, '20': 1, '10': 1 } }
  let(:error) do
    'ERROR: THE AMOUNT YOU REQUESTED CANNOT BE COMPOSED FROM BILLS /
    AVAILABLE IN THIS ATM. PLEASE ENTER A DIFFERENT AMOUNT:'
  end

  it 'returns expected hash' do
    expect(described_class.new(bills_hash, 100).call).to eq error
  end
end
